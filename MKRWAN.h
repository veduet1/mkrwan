/*
  This file is part of the MKRWAN library.
  Copyright (C) 2017  Arduino AG (http://www.arduino.cc/)

  Based on the TinyGSM library https://github.com/vshymanskyy/TinyGSM
  Copyright (c) 2016 Volodymyr Shymanskyy

  MKRWAN library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MKRWAN library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with MKRWAN library.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MKRWAN_H_DEFINED
#define MKRWAN_H_DEFINED

#include "Arduino.h"
#include "utilities.h"

#ifdef PORTENTA_CARRIER
#undef LORA_RESET
#define LORA_RESET (PD_5)
#undef LORA_BOOT0
#define LORA_BOOT0 (PJ_11)
#endif

constexpr unsigned long DEFAULT_JOIN_TIMEOUT = 60000;
constexpr unsigned long DEFAULT_MODEM_TIMEOUT = 1000;

#ifndef YIELD
#define YIELD() \
  { delay(2); }
#endif

using ConstStr = const char*;

template<class T>
const T& Min(const T& a, const T& b) {
  return (b < a) ? b : a;
}

template<class T>
const T& Max(const T& a, const T& b) {
  return (b < a) ? a : b;
}

#if !defined(LORA_RX_BUFFER)
#define LORA_RX_BUFFER 256
#endif

#define LORA_NL "\r"
static const char LORA_OK[] = "+OK";
static const char LORA_ERROR[] = "+ERR\r";
static const char LORA_ERROR_PARAM[] = "+ERR_PARAM\r";
static const char LORA_ERROR_BUSY[] = "+ERR_BUSY\r";
static const char LORA_ERROR_OVERFLOW[] = "+ERR_PARAM_OVERFLOW\r";
static const char LORA_ERROR_NO_NETWORK[] = "+ERR_NO_NETWORK\r";
static const char LORA_ERROR_RX[] = "+ERR_RX\r";
static const char LORA_ERROR_UNKNOWN[] = "+ERR_UNKNOWN\r";

static const char ARDUINO_FW_VERSION[] = "ARD-078 1.2.1";
static const char ARDUINO_FW_IDENTIFIER[] = "ARD-078";

enum class LoRaBand : int {
  AS923 = 0,
  AU915,
  CN470,
  CN779,
  EU433,
  EU868,
  KR920,
  IN865,
  US915,
  US915_HYBRID,
};

enum class RfMode : int {
  RFO = 0,
  PABOOST,
};

enum class LoraMode : int {
  ABP = 0,
  OTAA,
};

enum class LoraClass : char {
  CLASS_A = 'A',
  CLASS_B,
  CLASS_C,
};

class LoRaModem : public Stream {
  typedef SerialFifo<uint8_t, LORA_RX_BUFFER> RxFifo;

  Stream& stream;
  bool network_joined;
  RxFifo rx;
  RxFifo tx;
  String fw_version;
  unsigned long lastPollTime;
  unsigned long pollInterval;
  int mask_size;
  uint16_t channelsMask[6];
  String channel_mask_str;
  LoRaBand region;

public:
  LoRaModem(Stream &stream = (Stream&)Serial, unsigned long timeout = DEFAULT_MODEM_TIMEOUT);

  void lockFirmware();

  int joinOTAA(unsigned long timeout = DEFAULT_JOIN_TIMEOUT);
  
  // Stream compatibility (like UDP)
  void beginPacket();
  int endPacket(bool confirmed = false);

  // Implementation of Print Abstract methods
  size_t write(uint8_t c) override;
  size_t write(const uint8_t* buffer, size_t size) override;
  void flush() override;

  template<typename T> inline size_t write(T val) {
    return write((uint8_t*)&val, sizeof(T));
  }

  // Implementation of Stream abstract methods
  int available() override;
  int read() override;
  int peek() override;

  int read(uint8_t* buf, size_t size);

  uint8_t connected();

  /*
   * Basic functions
   */
  bool begin(LoRaBand band, uint32_t baud = 19200, uint16_t config = SERIAL_8N2);

  bool init();

  inline bool configureClass(LoraClass _class) {
    return setRegister("+CLASS", (char)_class);
  }

  inline bool configureBand(LoRaBand band) {
    if (!setRegister("+BAND", (int)band)) return false;
    if (band == LoRaBand::EU868 && isArduinoFW()) return dutyCycle(true);
    return true;
  }

  int getChannelMaskSize(LoRaBand band);

  String getChannelMask();

  int isChannelEnabled(int pos);

  bool disableChannel(int pos);

  bool enableChannel(int pos);

  bool sendMask();

  inline bool sendMask(String newMask) {
    return setRegister("+CHANMASK", newMask);
  }

  inline void setBaud(unsigned long baud) {
    setRegister("+UART", baud);
  }

  bool autoBaud(unsigned long timeout = 10000L);

  String version();

  inline String deviceEUI() {
    return getRegister("+DEVEUI");
  }

  inline void maintain() {
    while (stream.available()) {
      waitResponse(100);
    }
  }

  inline void minPollInterval(unsigned long secs) {
    pollInterval = secs * 1000;
  }

  void poll();

  inline bool factoryDefault() {
    sendAT("+FACNEW");  // Factory
    return waitResponse() == 1;
  }

  /*
   * Power functions
   */

  bool restart();

  bool power(RfMode mode, uint8_t transmitPower);

#ifdef SerialLoRa
  // Sends the modem into dumb mode, so the Semtech chip can be controlled directly
  // The only way to exit this mode is through a begin()
  void dumb();
#endif

  inline bool dutyCycle(bool on) {
    return setRegister("+DUTYCYCLE", on);
  }

  inline bool setPort(uint8_t port) {
    return setRegister("+PORT", port);
  }

  inline bool publicNetwork(bool publicNetwork) {
    return setRegister("+NWK", publicNetwork);
  }

  inline bool sleep(bool on = true) {
    return setRegister("+SLEEP", on);
  }

  inline bool format(bool hexMode) {
    return setRegister("+DFORMAT", hexMode);
  }

  /*
	DataRate 	Modulation 	SF 	BW 	bit/s
	0 	LoRa 	12 	125 	250
	1 	LoRa 	11 	125 	440
	2 	LoRa 	10 	125 	980
	3 	LoRa 	9 	125 	1'760
	4 	LoRa 	8 	125 	3'125
	5 	LoRa 	7 	125 	5'470
	6 	LoRa 	7 	250 	11'000
  */

  inline bool dataRate(uint8_t dr) {
    return setRegister("+DR", dr);
  }

  inline int getDataRate() {
    return getRegister("+DR").toInt();
  }

  inline bool setAppEui(String eui) {
    return setRegister("+APPEUI", eui);
  }

  inline bool setDeviceEui(String eui) {
    return setRegister("+DEVEUI", eui);
  }

  inline bool setAppKey(String key) {
    return setRegister("+APPKEY", key);
  }

  inline String getAppKey() {
    return getRegister("+APPKEY");
  }

  inline String getDevEUI() {
    return getRegister("+DEVEUI");
  }

  inline String getAppEUI() {
    return getRegister("+APPEUI");
  }

  template<typename T>
  inline bool readFromNVM(T &val, size_t from = 0) {
    return getNVMData(from, sizeof(T), (uint8_t*)&val);
  }

  template<typename T>
  inline bool writeToNVM(const T &val, size_t from = 0) {
    return setNVMData(from, sizeof(T), (const uint8_t*)&val);
  }
protected:
  bool getNVMData(size_t pos, size_t size, uint8_t *data);
  bool setNVMData(size_t pos, size_t size, const uint8_t *data);

  bool getNVMByte(size_t pos, uint8_t &byte);
  bool setNVMByte(size_t pos, uint8_t val);

  bool getRegister(ConstStr reg, String &val);
  bool setRegister(ConstStr reg, const String &val);

  template <typename T>
  inline bool setRegister(ConstStr reg, const T &val) {
    return setRegister(reg, String(val));
  }

  inline String getRegister(ConstStr reg) {
    String ret;

    if (!getRegister(reg, ret)) return "";

    return ret;
  }

private:
  inline bool isArduinoFW() {
    return (fw_version.indexOf(ARDUINO_FW_IDENTIFIER) >= 0);
  }

  inline bool isLatestFW() {
    return (fw_version == ARDUINO_FW_VERSION);
  }

  inline bool changeMode(LoraMode mode) {
    return setRegister("+MODE", (int)mode);
  }

  bool join(uint32_t timeout);

  /**
   * @brief transmit uplink
   * 
   * @param buff data to transmit
   * @param len length of the buffer
   * @param confirmed true = transmit confirmed uplink
   * @return int a positive number indicate success and is the number of bytes transmitted
   *             -1 indicates a timeout error
   *             -2 indicates LORA_ERROR
   *             -3 indicates LORA_ERROR_PARAM
   *             -4 indicates LORA_ERROR_BUSY
   *             -5 indicates LORA_ERROR_OVERFLOW
   *             -6 indicates LORA_ERROR_NO_NETWORK
   *             -7 indicates LORA_ERROR_RX
   *             -8 indicates LORA_ERROR_UNKNOWN
   *             -20 packet exceeds max length
   *             
   */
  int modemSend(const void* buff, size_t len, bool confirmed);

  size_t modemGetMaxSize();

  size_t getJoinStatus();

  /* Utilities */
  template<typename T>
  void streamWrite(T last) {
    stream.print(last);
  }

  template<typename T, typename... Args>
  void streamWrite(T head, Args... tail) {
    stream.print(head);
    streamWrite(tail...);
  }

  inline int streamRead() {
    return stream.read();
  }

  bool streamSkipUntil(char c, unsigned long timeout);

  inline bool streamSkipUntil(char c) {
    return streamSkipUntil(c, getTimeout());
  }

  template<typename... Args>
  void sendAT(Args... cmd) {
    streamWrite("AT", cmd..., LORA_NL);
    stream.flush();
    YIELD();
    debug("### AT: ", cmd...);
  }

  size_t waitResponse(unsigned long timeout, String &data, ConstStr args[], size_t args_size);

  size_t waitResponse(unsigned long timeout, ConstStr arg1 = LORA_OK, ConstStr arg2 = LORA_ERROR) {
    String data;
    ConstStr args[2] = { arg1, arg2 };

    return waitResponse(timeout, data, args, 2);
  }

  size_t waitResponse(ConstStr arg1 = LORA_OK, ConstStr arg2 = LORA_ERROR) {
    return waitResponse(getTimeout(), arg1, arg2);
  }
};

#endif // MKRWAN_H_DEFINED
