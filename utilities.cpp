#include "utilities.h"

bool isHexaC(char c) {
  return (c >= '0' && c <= '9') ||
         (c >= 'a' && c <= 'f') ||
         (c >= 'A' && c <= 'F');
}

bool checkHexaStr(const String &val, size_t len) {
  if (2*len != val.length()) return false;

  size_t calc;

  for (calc = 0; calc < val.length(); calc++) {
    if (!isHexaC(val[calc])) return false;
  }

  return true;
}

SkippedString::SkippedString(String &parent) : parent(parent) {}

size_t SkippedString::printTo(Print& p) const {
  size_t ret = 0;

  for (size_t i = 0; i < parent.length(); ++i) {
    uint8_t c = parent[i];

    if (c >= 0x20 && c <= 0x7E) {
      if (c == '\"' || c == '\'' || c == '\?' || c == '\\') {
        ret += p.write('\\');
      }
      ret += p.write(c);
    } else {
      switch(c) {
      case '\a':
        ret += p.write("\\a");
        break;
      case '\b':
        ret += p.write("\\b");
        break;
      case '\e':
        ret += p.write("\\e");
        break;
      case '\f':
        ret += p.write("\\f");
        break;
      case '\n':
        ret += p.write("\\n");
        break;
      case '\r':
        ret += p.write("\\r");
        break;
      case '\t':
        ret += p.write("\\t");
        break;
      case '\v':
        ret += p.write("\\v");
        break;
      default:
        ret += p.write("\\x");
        ret += p.print((int)c, 16);
      }
    }
  }

  return ret;
}
