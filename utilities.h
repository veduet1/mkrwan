#ifndef UTILITIES_H_INCLUDED
#define UTILITIES_H_INCLUDED

#include "Arduino.h"

#define SERIAL_DEBUG Serial

#ifdef SERIAL_DEBUG
template<typename T>
static void debug(T last) {
  SERIAL_DEBUG.println(last);
}

template<typename T, typename... Args>
static void debug(T head, Args... tail) {
  SERIAL_DEBUG.print(head);
  debug(tail...);
}
#else
#define debug(...)
#endif

template<typename T>
static void print(T last) {
  Serial.println(last);
}

template<typename T, typename... Args>
static void print(T head, Args... tail) {
  Serial.print(head);
  print(tail...);
}

template<class T, unsigned N>
class SerialFifo {
  T buffer[N];

  size_t in_idx, out_idx;

public:
  inline SerialFifo() : in_idx(0), out_idx(0) {}

  inline void clear() {
    in_idx = 0;
    out_idx = 0;
  }

  inline bool is_writeable() const {
    return free_space() > 0;
  }

  inline bool is_readable(void) const {
    return out_idx != in_idx;
  }

  size_t free_space() const {
    size_t remaining = out_idx - in_idx;

    if (remaining <= 0) remaining += N;

    return remaining - 1;
  }

  size_t size(void) const {
    int size = in_idx - out_idx;

    if (size < 0) size += N;
  
    return size;
  }

  bool push(const T &to_put) {
    if (!is_writeable()) return false;
    
    buffer[in_idx] = to_put;

    in_idx ++;

    if (in_idx >= N) in_idx = 0;

    return true;
  }

  size_t push(const T* to_put, size_t size) {
    if (to_put == nullptr) return 0;

    for (size_t i = 0; i < size; i++) {
      if (!push(to_put[i])) return i;
    }

    return size;
  }

  bool pop(T &element) {
    if (!is_readable()) return false;

    element = buffer[out_idx];

    out_idx ++;

    if (out_idx > N) out_idx = 0;
    
    return true;
  }

  size_t pop(T *element, size_t size) {
    if (element == nullptr) return 0;

    for (size_t i = 0; i < size; i++) {
      if (!pop(element[i])) return i;
    }

    return size;
  }

  bool get(T &element) const {
    if (!is_readable()) return false;

    element = buffer[out_idx];
    
    return true;
  }
};

bool checkHexaStr(const String &val, size_t len);

class SkippedString : public Printable {
  String &parent;

public:
  SkippedString() = delete;
  SkippedString(String &parent);

  size_t printTo(Print& p) const override;
};

#endif // UTILITIES_H_INCLUDED
