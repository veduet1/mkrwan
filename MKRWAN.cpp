#include "MKRWAN.h"

constexpr size_t RESPONSE_TIMEOUT = -1;
constexpr size_t RESPONSE_OK = 0;

#ifdef SerialLoRa
LoRaModem::LoRaModem(Stream &, unsigned long timeout):
  stream(SerialLoRa),
#else
LoRaModem::LoRaModem(Stream &stream, unsigned long timeout):
  stream(stream),
#endif
  network_joined(false),
  lastPollTime(millis()),
  pollInterval(300000),
  mask_size(0),
  channelsMask{0},
  region(LoRaBand::EU868)
{
  setTimeout(timeout);
}

void LoRaModem::lockFirmware() {
  YIELD();
  sendAT("$APKACCESS");
}

int LoRaModem::joinOTAA(unsigned long timeout) {
  YIELD();
  rx.clear();
  changeMode(LoraMode::OTAA);
  network_joined = join(timeout);
  delay(1000);
  return network_joined;
}

void LoRaModem::beginPacket() {
  tx.clear();
}

int LoRaModem::endPacket(bool confirmed) {
  uint8_t buffer[LORA_RX_BUFFER];
  memset(buffer, 0, LORA_RX_BUFFER);
  size_t size = tx.pop(buffer, LORA_RX_BUFFER);
  return modemSend(buffer, size, confirmed);
}

size_t LoRaModem::write(uint8_t c) {
  return tx.push(c);
};

size_t LoRaModem::write(const uint8_t* buffer, size_t size) {
  return tx.push(buffer, size);
};

void LoRaModem::flush() {
  stream.flush();
}

int LoRaModem::available() {
  YIELD();
  if (!rx.size()) {
    maintain();
  }
  return (int)rx.size();  // + buf_available;
}

int LoRaModem::read() {
  uint8_t c;
  if (read(&c, 1) == 1) {
    return c;
  }
  return -1;
}

int LoRaModem::peek() {
  uint8_t c;
  if (rx.get(c)) {
    return c;
  }
  return -1;
}

int LoRaModem::read(uint8_t* buf, size_t size) {
  YIELD();
  maintain();
  size_t cnt = 0;
  while (cnt < size) {
    size_t chunk = Min(size - cnt, rx.size());
    if (chunk > 0) {
      rx.pop(buf, chunk);
      buf += chunk;
      cnt += chunk;
      continue;
    }
    // TODO: Read directly into user buffer?
    maintain();
    /*
    if (buf_available > 0) {
      modemRead(rx.free());
    } else {
      break;
    }
    */
  }
  return (int)cnt;
}

uint8_t LoRaModem::connected() {
  if (available()) {
    return true;
  }
  return network_joined;
}

#ifdef SerialLoRa
void initializeUART(uint32_t baud, uint16_t config) {
  SerialLoRa.begin(baud, config);
  pinMode(LORA_BOOT0, OUTPUT);
  digitalWrite(LORA_BOOT0, LOW);
  pinMode(LORA_RESET, OUTPUT);
  digitalWrite(LORA_RESET, HIGH);
  delay(200);
  digitalWrite(LORA_RESET, LOW);
  delay(200);
  digitalWrite(LORA_RESET, HIGH);
  delay(200);
}
#else
void initializeUART(uint32_t, uint16_t) {}
#endif

bool LoRaModem::begin(LoRaBand band, uint32_t baud, uint16_t config) {
  initializeUART(baud, config);
  region = band;
  if (init()) {
    return configureBand(band);
  } else {
    debug("Trying using SERIAL_8N1");
    initializeUART(baud, SERIAL_8N1);
    return init();
  }
}

bool LoRaModem::init() {
  if (!autoBaud()) {
    return false;
  }
  // populate version field on startup
  version();
  if (!isLatestFW()) {
    debug("Please update fw using MKRWANFWUpdate_standalone.ino sketch");
  }
  return true;
}

int LoRaModem::getChannelMaskSize(LoRaBand band) {
  switch (band) {
    case LoRaBand::AS923:
    case LoRaBand::CN779:
    case LoRaBand::EU433:
    case LoRaBand::EU868:
    case LoRaBand::KR920:
    case LoRaBand::IN865:
      mask_size = 1;
      break;
    case LoRaBand::AU915:
    case LoRaBand::CN470:
    case LoRaBand::US915:
    case LoRaBand::US915_HYBRID:
      mask_size = 6;
      break;
    default:
      break;
  }
  return mask_size;
}

String LoRaModem::getChannelMask() {
  int size = 4 * getChannelMaskSize(region);

  if (getRegister("+CHANMASK", channel_mask_str)) {
    debug("Full channel mask string: ", channel_mask_str);
    sscanf(channel_mask_str.c_str(), "%04hx%04hx%04hx%04hx%04hx%04hx", &channelsMask[0], &channelsMask[1], &channelsMask[2],
            &channelsMask[3], &channelsMask[4], &channelsMask[5]);

    return channel_mask_str.substring(0, size);
  }

  return "0";
}

int LoRaModem::isChannelEnabled(int pos) {
  //Populate channelsMask array
  int max_retry = 3;
  int retry = 0;
  while (retry < max_retry) {
    String mask = getChannelMask();
    if (mask != "0") {
      break;
    }
    retry++;
  }

  int row = pos / 16;
  int col = pos % 16;
  auto channel = (uint16_t)(1 << col);

  channel = ((channelsMask[row] & channel) >> col);

  return channel;
}

bool LoRaModem::disableChannel(int pos) {
  //Populate channelsMask array
  int max_retry = 3;
  int retry = 0;
  while (retry < max_retry) {
    String mask = getChannelMask();
    if (mask != "0") {
      break;
    }
    retry++;
  }

  int row = pos / 16;
  int col = pos % 16;
  uint16_t mask = ~(uint16_t)(1 << col);

  channelsMask[row] = channelsMask[row] & mask;

  return sendMask();
}

bool LoRaModem::enableChannel(int pos) {
  //Populate channelsMask array
  int max_retry = 3;
  int retry = 0;
  while (retry < max_retry) {
    String mask = getChannelMask();
    if (mask != "0") {
      break;
    }
    retry++;
  }

  int row = pos / 16;
  int col = pos % 16;
  auto mask = (uint16_t)(1 << col);

  channelsMask[row] = channelsMask[row] | mask;

  return sendMask();
}

bool LoRaModem::sendMask() {
  String newMask;

  /* Convert channel mask into string */
  for (int i = 0; i < 6; i++) {
    char hex[5];
    sprintf(hex, "%04x", channelsMask[i]);
    newMask.concat(hex);
  }

  debug("Newmask: ", newMask);

  return sendMask(newMask);
}

bool LoRaModem::autoBaud(unsigned long timeout) {
  for (unsigned long start = millis(); millis() - start < timeout;) {
    sendAT("");
    if (waitResponse(getTimeout()) == RESPONSE_OK) {
      delay(100);
      return true;
    }
    delay(100);
  }
  return false;
}

String LoRaModem::version() {
  String devId, ver;

  if(!getRegister("+DEV", devId)) return "";
  if(!getRegister("+VER", ver)) return "";

  fw_version = devId + " " + ver;

  return fw_version;
}

void LoRaModem::poll() {
  if (millis() - lastPollTime < pollInterval) return;
  lastPollTime = millis();
  // simply trigger a fake write
  uint8_t dummy = 0;
  modemSend(&dummy, 1, true);
}

bool LoRaModem::restart() {
  if (!autoBaud()) {
    return false;
  }
  sendAT("+REBOOT");
  if (waitResponse(10000L, "+EVENT=0,0") != RESPONSE_OK) {
    return false;
  }
  delay(1000);
  return init();
}

bool LoRaModem::power(RfMode mode, uint8_t transmitPower) {  // transmitPower can be between 0 and 5
  sendAT("+RFPOWER=", (int)mode, ",", transmitPower);
  if (waitResponse() != RESPONSE_OK) {
    return false;
  } else {
    String resp = stream.readStringUntil('\r');
  }
  return true;
}

#ifdef SerialLoRa
  // Sends the modem into dumb mode, so the Semtech chip can be controlled directly
  // The only way to exit this mode is through a begin()
  void LoRaModem::dumb() {
    SerialLoRa.end();
    pinMode(LORA_IRQ_DUMB, OUTPUT);
    digitalWrite(LORA_IRQ_DUMB, LOW);

    // Hardware reset
    pinMode(LORA_BOOT0, OUTPUT);
    digitalWrite(LORA_BOOT0, LOW);
    pinMode(LORA_RESET, OUTPUT);
    digitalWrite(LORA_RESET, HIGH);
    delay(200);
    digitalWrite(LORA_RESET, LOW);
    delay(200);
    digitalWrite(LORA_RESET, HIGH);
    delay(50);

    // You can now use SPI1 and LORA_IRQ_DUMB as CS to interface with the chip
  }
#endif

bool LoRaModem::getNVMData(size_t pos, size_t size, uint8_t *data) {
  if (data == nullptr) {
    debug("ERR: Trying to read to a null pointer");
    return false;
  }

  if ((pos + size) < 64) {
    for (size_t i = 0; i < size; i++) {
      if (!getNVMByte(pos + i, data[i])) return false;
    }

    return true;
  }

  debug("ERR: Reading the value from NVM needs a max index of ", pos + size, ". However the NVM size is 64");

  return false;
}

bool LoRaModem::setNVMData(size_t pos, size_t size, const uint8_t *data) {
  if (data == nullptr) {
    debug("ERR: Trying to read to a null pointer");
    return false;
  }

  if ((pos + size) < 64) {
    for (size_t i = 0; i < size; i++) {
      if (!setNVMByte(pos + i, data[i])) return false;
    }

    return true;
  }

  debug("ERR: Writing the value to NVM needs a max index of ", pos + size, ". However the NVM size is 64");

  return false;
}

bool LoRaModem::getNVMByte(size_t pos, uint8_t &byte) {
  if (pos < 64) {
    sendAT("$NVM ", pos);

    if (waitResponse("+OK=") == RESPONSE_OK) {
      byte = stream.readStringUntil('\r').toInt();
      debug("Received: ", byte);
      return true;
    }
  }

  debug("ERR: Unable to set value at position ", pos);

  return false; 
}

bool LoRaModem::setNVMByte(size_t pos, uint8_t val) {
  if (pos < 64) {
    sendAT("$NVM ", pos, ",", val);

    if (waitResponse() == RESPONSE_OK) return true;
  }

  debug("ERR: Unable to set value ", val, " to register ", pos);

  return false;
}

bool LoRaModem::getRegister(ConstStr reg, String &val) {
  sendAT(reg, '?');

  switch (waitResponse("+OK=", "+ERR")) {
  case RESPONSE_OK:
    val = stream.readStringUntil('\r');
    return true;
  case RESPONSE_TIMEOUT:
    debug("ERR: The device does not respond at time");
    return false;
  default:
#ifdef SERIAL_DEBUG
    String error = stream.readStringUntil('\r');
    if (error.length() && error[0] == '=') {
      int errorCode = error.substring(1).toInt();

      debug("ERR: The command respond with code ", errorCode);
    } else {
      debug("ERR: Unknown error");
    }
#else
    streamSkipUntil('\r');
#endif
    return false;
  }
}

bool LoRaModem::setRegister(ConstStr reg, const String &val) {
  sendAT(reg, '=', val);

  switch (waitResponse("+OK", "+ERR")) {
  case RESPONSE_OK:
    return true;
  case RESPONSE_TIMEOUT:
    debug("ERR: The device does not respond at time");
    return false;
  default:
#ifdef SERIAL_DEBUG
    String error = stream.readStringUntil('\r');
    if (error.length() && error[0] == '=') {
      int errorCode = error.substring(1).toInt();

      debug("ERR: The command respond with code ", errorCode);
    } else {
      debug("ERR: Unknown error");
    }
#else
    streamSkipUntil('\r');
#endif
    return false;
  }
}

bool LoRaModem::join(uint32_t timeout) {
  sendAT("+JOIN");
  sendAT();
  return waitResponse(timeout, "+EVENT=1,1") == RESPONSE_OK;
}

int LoRaModem::modemSend(const void* buff, size_t len, bool confirmed) {
  size_t max_len = modemGetMaxSize();
  if (len > max_len) {
    return -20;
  }

  if (confirmed) {
    sendAT("+CTX ", len);
  } else {
    sendAT("+UTX ", len);
  }

  stream.write((uint8_t*)buff, len);

  String data;
  ConstStr arguments[8] = {
    LORA_OK,
    LORA_ERROR,
    LORA_ERROR_PARAM,
    LORA_ERROR_BUSY,
    LORA_ERROR_OVERFLOW,
    LORA_ERROR_NO_NETWORK,
    LORA_ERROR_RX,
    LORA_ERROR_UNKNOWN
  };

  size_t rc = waitResponse(getTimeout(), data, arguments, 8);

  if (rc == RESPONSE_TIMEOUT) {  ///< timeout
    return -1;
  } else if (rc == RESPONSE_OK) {  ///< OK
    return (int)len;
  } else {  ///< LORA ERROR
    return -(int)rc-1;
  }
}

size_t LoRaModem::modemGetMaxSize() {
  if (isArduinoFW()) return 64;

  String ret;

  if (getRegister("+MSIZE", ret)) {
    return ret.toInt();
  }

  return 0;
}

size_t LoRaModem::getJoinStatus() {
  String ret;

  if (getRegister("+NJS", ret)) {
    return ret.toInt();
  }

  return 0;
}

bool LoRaModem::streamSkipUntil(char c, unsigned long timeout) {
  unsigned long end = millis() - timeout;
  while (millis() < end) {
    while (!stream.available() && millis() < end) {}
    if (stream.read() == c)
      return true;
  }
  return false;
}

size_t LoRaModem::waitResponse(unsigned long timeout, String &data, ConstStr args[], size_t args_size) {
  data.reserve(64);
  size_t index = RESPONSE_TIMEOUT;
  size_t length = 0;
  const unsigned long endMillis = timeout + millis();

  do {
    YIELD();

    while (index >= args_size && stream.available() > 0) {
      int val = streamRead();
      if (val < 0) continue;
      data += (char)val;

      for (size_t i = 0; i < args_size; i++) {
        if (data.endsWith(args[i])) {
          index = i;
          break;
        }
      }

      if (index >= args_size && data.endsWith("+RECV=")) {
        data = "";
        
        // Ignoring the port number
        streamSkipUntil(',');

        length = stream.readStringUntil('\r').toInt();

        streamSkipUntil('\n');
        streamSkipUntil('\n');

        for (size_t i = 0; i < length;) {
          if (stream.available()) {
            int c = stream.read();

            if (c >= 0) {
              rx.push((char)c);
              i++;
            }
          }
        }
      }
    }
  } while (index >= args_size && millis() < endMillis);

  if (index >= args_size) {
    data.trim();

    if (data.length()) {
      debug("### Unhandled: ", SkippedString(data));
      index = args_size;
    } else {
      debug("### Timeout when waiting response");
      index = RESPONSE_TIMEOUT;
    }
    
    data = "";
  } else if (data.length()) {
    debug("### Index ", index, " with packet `", SkippedString(data), "`");
  }

  return index;
}
